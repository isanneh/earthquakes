#Author: Isatou Sanneh
#Email: isanneh00@citymail.cuny.edu

from flask import Flask, render_template, url_for, request

import requests
import json

app = Flask(__name__)


@app.route('/search', methods = ['GET', 'POST'])
def search():
    """ This page returns an error message if the location entered is not found.  If the location is found, it returns a map and plots any   		earthquakes that occured within the specified region
    """
    if request.method == 'GET':
        location = request.args.get('address', '')

    return render_template('search.html', location = location)

@app.route('/', methods = ['GET', 'POST'])
def home():
    """ This page contains a user input searchbox and also displays the recent top ten largest earthquakes """
    return render_template('home.html')



@app.route('/location', methods = ['GET', 'POST'])
def location():
    """ This page takes an input location entered by the user and passes it to the search page for further processing """
    return render_template('location.html')


@app.route('/top_earthquakes')
def top_earthquakes():
    """ This page displays a list of ten largest earthquakes that occurred in the past 12 months.  
    Two reverse geocode datasets were used to find the location of the earthquake using latitude and longitude coordinates.
 1: This dataset is used to return the country.
 2. This dataset is used to return the nearest ocean to the earthquake location.  This dataset is only used if no results were found using dataset 1  """
    return render_template('top_earthquakes.html')



"""
This page displays the country where an earthquake occured.  It returns the latitude and longitude values if the reverse geocode lookup didn't return any result 
"""
@app.route('/loadLocation', methods = ['GET', 'POST'])
def loadLocation():
    if request.method == 'GET':

        lat = request.args.get('lat', '')
        lng = request.args.get('lng', '')
	url = "http://api.geonames.org/findNearbyPlaceNameJSON?lat=" + lat + "&lng=" + lng + "&username=isanneh"


	


        r = requests.get(url)

	print 'r', r.content

	a = 1
	data = json.loads(r.content)['geonames']
	if len(data) == 0:
	    earthquake_location = str(lat) + ',' + str(lng) + '(latitude, longitude)'
	else:
            earthquake_location = data[0]['countryName']


	
    return render_template('loadLocation.html', earthquake_location = earthquake_location)


@app.route('/loadLocation2', methods = ['GET', 'POST'])
def loadLocation2():
    return render_template('loadLocation.html')




if __name__ == '__main__':
    app.run()










